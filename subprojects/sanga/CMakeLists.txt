cmake_minimum_required(VERSION 3.0)

project(sanga LANGUAGES C VERSION 0.1.1)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
find_package(Guile REQUIRED)


# make directory sanga for all the header files
file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/sanga)

include_directories(
  ${GUILE_INCLUDE_DIR}
  ${CMAKE_SOURCE_DIR}/src/include;
  ${CMAKE_BINARY_DIR};
  )

add_subdirectory(${CMAKE_SOURCE_DIR}/src/function)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/dom)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/print)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/section)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/backend)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/parser)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/lang)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/guile)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/validate)

add_executable(sangai
  ${FLEX_SANGA_LEXER_OUTPUTS}
  ${BISON_SANGA_PARSER_OUTPUTS}
  src/main.c
)

target_link_libraries(sangai
  ${GUILE_LIBRARIES}
  sangaparser
  sangasection
  sangahtml
  sangafunction
  sangadom
  sangaprint
  sangavalidate
  sangalang
  sangaast
  )

set_property(TARGET sangai PROPERTY C_STANDARD 11)


file(GLOB HEADERS ${CMAKE_SOURCE_DIR}/src/include/sanga/*.h)
file(GLOB GENERATED_HEADERS ${CMAKE_BINARY_DIR}/sanga/*.h)
install(FILES ${HEADERS} ${GENERATED_HEADERS}
  DESTINATION /usr/local/include/sanga)

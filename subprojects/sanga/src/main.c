/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/




#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <libguile.h>

#include <sanga/parser.tab.h>
#include <sanga/section.h>
#include <sanga/print.h>
#include <sanga/html.h>
#include <sanga/dom.h>
#include <sanga/validate.h>
#include <sanga/lang.h>
#include <sanga/guile.h>

extern FILE* yyin;

int main (int argc, char **argv)
{
  struct DOM *dom1, *dom2;
  if (argc != 3)
    return -1;
  
  dom1 = parse(argv[1]);
  dom2 = parse(argv[2]);
  
  printf("लेखको नाम : %s \n", dom1->root->functions->functions[1].argument);
  printf("लेखको नाम : %s \n", dom2->root->functions->functions[1].argument);  
  
  free_AST(dom1);
  free_AST(dom2);
  
  return 0;
}

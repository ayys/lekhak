 /* LEXICAL ANALYZER FOR सांगा */
%{
#include <stdlib.h>

#include "parser.tab.h"
#include <sanga/section.h>

char *section_name = NULL;
char *section_body = NULL;

int braces_balance = 0;
int inside_function_block = 0;
/*
+1 to braces_balance on every BRACE_OPEN
-1 to brace_balance on every BRACE_CLOSE
*/


%}

%option noyywrap


%s section_name_state
%x section_name_start_state
%s section_body_state
%s section_end_state

%%

<<EOF>>    {return EOL;} /* EXIT ON CTRL-D */


<INITIAL>"["         {		/*start section*/
              braces_balance++;
              BEGIN(section_name_state);
              return BRACE_OPEN;
            }

<INITIAL>\n         { return EOL; }



<section_name_state>{
    "{" { inside_function_block = 1; return FUNCTION_BRACE_OPEN; }
    [ \t\n\r]+ {} /*ignore whitespace while in section name state*/
    (\'(\\.|[^\'])+\')|(\"(\\.|[^\"])+\")|[^ \r\t\n\\:}\{\]\[]+ { /*get non-whitespace, except ":", "{", "}",
                                                                    or get quoted strings with whitespace */
                                  section_name = malloc(SECTION_NAME_LENGTH);
                                  /* SECTION_NAME_LENGTH defined in section.h */
                                  /*If it starts with a quote, it ends with one*/
                                  if (yytext[0] == '\"' || yytext[0] == '\''){ /*Dont want quotes*/
                                      yytext[strlen(yytext) - 1] = '\0';
                                      strcpy(section_name, &yytext[1]);
                                  } else strcpy(section_name, yytext);
                                  yylval.string = section_name;
                                  if (inside_function_block == 1)
                                      BEGIN(section_name_state);
                                  else{
                                      BEGIN(section_body_state);
                                      inside_function_block = 0;
                                  }
                                  return SECTION_WORD;
                             }

    ":" { return FUNCTION_EQUALS_TO; }
    "}"  { inside_function_block = 0; BEGIN(section_body_state); return FUNCTION_BRACE_CLOSE;  }
}

<section_body_state>("\\["|"\\]"|[^\[\]])+ {
                                              section_body = malloc(BODY_SIZE);
                                              /* BODY_SIZE DEFINED IN STRUCTURE.H */
                                              /* IGNORE THE FIRST CHARACTER, WHICH IS A SPACE.
                                                 THIS IS DONE SINCE A SPACE IS USED TO SEPERATE
                                                  THE SECTION NAME AND THE BODY IN THE PARSER*/
                                              strcpy(section_body, yytext);
                                              yylval.string = section_body;
                                              buffer_length = strlen(section_body);
                                              BEGIN(INITIAL);
                                              return BODY;
                                           } /* SECTION BODY */

<section_body_state,INITIAL>"]"         {
              braces_balance--;
              if (braces_balance > 0)
                  BEGIN(section_body_state);
              else
                  BEGIN(INITIAL);
              return BRACE_CLOSE;
            }


%%

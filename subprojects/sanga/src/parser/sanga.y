%{
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <sanga/dom.h>
#include <sanga/section.h>
#include <sanga/function.h>

#define YYDEBUG 1 		/*enable debug*/

/* NECESSARY TO IGNORE FUNCTION NOT DECLARED WARNINGS */
int yylex (void);
int yyerror (const char *);

long int current_depth = -1, stable_depth = -1;

extern void print_section (section*);
section *parent_sec, *current_sec;
/* section *root_sec, *current_section IS DEFINED IN section.h */

%}

%union {
    struct section* section;
    struct function_list* function_list;
    char* string;
}

%token EOL

%token BODY SECTION_WORD
%token FUNCTION_BRACE_OPEN FUNCTION_BRACE_CLOSE
%token FUNCTION_EQUALS_TO
%token BRACE_OPEN BRACE_CLOSE

%type	<section>	section
%type	<string>	BODY SECTION_WORD section_body
%type	<string>	section_functions
%type	<function_list>	section_function_block

%define parse.error verbose

%% /* GRAMMAR RULES */

line:		EOL             {buffer_length = 0; return 1; }
	|       section  EOL    {buffer_length = 0; return 0; }
		;

section:	BRACE_OPEN
		{
		    section *tmp = create_section();
		    parent_sec = current_sec;
		    current_sec = tmp;
		    /* root_sec HAS NO PARENT */
		    if (parent_sec == NULL)
			{
			    dom->root = tmp;
			}
		    else
			{
			    section_add_child(parent_sec, current_sec);
			}
		    $<section>$ = tmp;
		}
	        section_function_block section_body BRACE_CLOSE
		{
		    update_section(current_sec, $4, parent_sec);

		    if (parent_sec != NULL)
			parent_sec = parent_sec->parent;
		    if (current_sec->parent != NULL)
			current_sec = current_sec->parent;
		    $$ = current_sec;
		}
	;

section_function_block:
		FUNCTION_BRACE_OPEN section_functions FUNCTION_BRACE_CLOSE {}
	|	section_function {}
	;

section_functions:
		section_function {}
|	section_functions section_function {}
	;

section_function:
		SECTION_WORD
		{add_function(current_sec->functions, $1, NULL);}
		|	SECTION_WORD FUNCTION_EQUALS_TO SECTION_WORD
		{ add_function(current_sec->functions, $1, $3);}
;


section_body:	BODY {$$ = $1;}
	|	section { $$ = NULL; }
	|	section_body section { $$ = $1; }
	|	section_body BODY
		{
		    if ($1 != NULL)
			{
			    strcat($1, $2);
			    free($2);
			}
		    $$ = $1;
		}
	;

%%

int yyerror (const char *s)
    {
	fprintf(stderr, "%s\n", s);
	return -1; /* RETURNS -1 IF THERE IS AN ERROR */
    }

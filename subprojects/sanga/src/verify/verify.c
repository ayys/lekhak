/*
  Verify that the "section title names" are keywords
  defined by trh language.
 */

#include <string.h>

#include <sanga/verify.h>
#include <sanga/section.h>
#include <sanga/function.h>

#define SANGA_KEYWORDS_NUM 4
/*
  array containing all the keywords in sanga

  PS. If you update this array, do also update the
  length_keywords variable with the new length of
  array.
*/
/*
  structure is as follows:
  First element is the number of attributes the section can have
  from element 2 is all the attributes possible
*/


struct keyword{
  unsigned int attr_n;
  const char* name;
  const char* const * attributes;
};

typedef struct keyword keyword;

keyword sanga_keywords[SANGA_KEYWORDS_NUM] = {
                              {.attr_n=9,
                               .name="लेख",
                               .attributes=(const char* const []){"शीर्षक", "लेखकहरू",
                                            "प्रकाशकको नाम",
                                            "प्रकाशकको ठेगाना",
                                            "प्रकाशनको मिति",
                                            "संस्करण", "श्रीङ्खला",
                                            "असिकिमु", "विषय-वस्तु"}},
                              {.attr_n=6,
                               .name="पाठ",
                               .attributes=(const char* const []){"शीर्षक", "लेखकहरू",
                                            "प्रकाशकको नाम",
                                            "प्रकाशकको ठेगाना",
                                            "प्रकाशकको मिति", "विषय-वस्तु"}},
                              {.attr_n=2,
                               .name="खण्ड",
                               .attributes=(const char* const []){"नाम", "अंकित"}},
                              {.attr_n=2,
                               .name="उपखण्ड",
                               .attributes=(const char* const []){"नाम", "अंकित"}}
};

int verify_function_name(char* section_name)
{
  for(int i = 0; i < SANGA_KEYWORDS_NUM; i++)
    {
      if (strcmp(section_name, sanga_keywords[i].name) == 0)
        return 0;
    }
  return -1;
}

keyword get_keyword_with_name(char* name)
{
  /* get the struct with name specified */
  for (int i = 0; i < SANGA_KEYWORDS_NUM; i++)
    {
      if (strcmp(name, sanga_keywords[i].name) == 0)
        {
          return sanga_keywords[i];
        }
    }
}

int verify_function_attribute(char* attribute, char* name)
{
  /* return 0 if attribute is valid, else return -1 */
  keyword kwrd = get_keyword_with_name(name);
  for(int i=0; i < kwrd.attr_n; i++)
    {
      if (strcmp(attribute, kwrd.attributes[i]) == 0)
        {
          return 0;
        }
    }
  return -1;
}

int verify_single_section(section* sec)
{
  if (verify_function_name(sec->functions->functions[0].name) != 0)
    return -1;
  if (sec->functions->nfunctions > 1)
    {
      for(int i = 1; i < sec->functions->nfunctions; i++)
        {
          int ret_val = verify_function_attribute(sec->functions->functions[i].name,
                                                  sec->functions->functions[0].name);
          if (ret_val != 0)
            return -1;
        }
    }
  return 0;
}


int verify_section(section* sec)
{
  return apply_to_each_section(sec, verify_single_section);
}

/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/




#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <sanga/section.h>
#include <sanga/html.h>
#include <sanga/function.h>

#define HTML_BEGIN_TAG_PADDING 2 /* for < & > */
#define HTML_END_TAG_PADDING 3 /* for </ & > */

/* private function */
char* merge_into_body(char *src, char *dest, unsigned long int offset)
{
  unsigned long int dest_len = strlen(dest);
  unsigned long int src_len = strlen(src);
  if (offset > dest_len)
    return 0; 			/* offset is greater than dest_len */
  char *dest_left, *dest_right, *output;
  dest_left = malloc(offset + 1);
  memset(dest_left, 0, offset + 1);
  dest_right = malloc(dest_len - offset + 1);
  memset(dest_right, 0, dest_len - offset + 1);

  strncat(dest_left, dest, offset);
  strncat(dest_right, dest + offset, dest_len - offset);

  output = malloc(dest_len + src_len + 1);
  memset(output, 0, dest_len + src_len + 1);
  strcat(output, dest_left);
  strcat(output, src);
  strcat(output, dest_right);
  free(dest_left);
  free(dest_right);
  return output;
}


/* private function */
void make_html_tags_from_function_list(function_list *func_lst,
				       char **begin_tags,
				       char **end_tags)
/* taks a function list from a section, and converts the
   functions in the list to html tags. The tags are stored
   in the two strings, begin_tags and end_tags.
   begin_tags contains the starting of the tags for eg. <html>
   end_tags contains the ending of the tags for eg. </html>
*/
{
  if (func_lst == NULL) return;

  struct function *tmp_func = &(func_lst->functions[0]);
  char *tmp_begin_tags = NULL, *tmp_end_tags = NULL;
  int len_begin_tags, len_end_tags;

  int func_counter;
  for(func_counter = 0; func_counter < func_lst->nfunctions; func_counter++)
    {
      /* create the beginning of tag */
      /* Added +1 to len_begin_tags and len_end_tags for the terminating */
      /* character in the string. */
      len_begin_tags = strlen(tmp_func->name) + HTML_BEGIN_TAG_PADDING + 1;
      if (tmp_begin_tags)
        {
         len_begin_tags += strlen(tmp_begin_tags);
        }

      if (tmp_begin_tags == NULL)
        len_begin_tags += 1;	/* FOR TERMINATING CHARACTER */
      if (!tmp_begin_tags)
        {
          tmp_begin_tags = malloc(len_begin_tags);
          memset(tmp_begin_tags, 0, len_begin_tags);
        }
      else
        {
          unsigned int tmp_len = strlen(tmp_begin_tags);
          tmp_begin_tags = realloc(tmp_begin_tags, len_begin_tags);

        }
      strncat(tmp_begin_tags, "<", 2);
      strncat(tmp_begin_tags, tmp_func->name, strlen(tmp_func->name));
      strncat(tmp_begin_tags, ">", 2);

      /* create the end of tag */
      unsigned int len_old_end_tags = tmp_end_tags?strlen(tmp_end_tags):0;
      /* Added +1 to len_begin_tags and len_end_tags for the terminating */
      /* character in the string. */
      len_end_tags = strlen(tmp_func->name) + HTML_END_TAG_PADDING + 1;
      if (tmp_end_tags)
        {
         len_end_tags += strlen(tmp_end_tags);
        }
      if (tmp_end_tags == NULL)
        len_end_tags += 1;	/* FOR TERMINATING CHARACTER */
      if (!tmp_end_tags)
        {
          tmp_end_tags = malloc(len_end_tags);
          memset(tmp_end_tags, 0, len_end_tags);
        }
      else
        {
          tmp_end_tags = realloc(tmp_end_tags, len_end_tags);
          memcpy(&tmp_end_tags[len_end_tags - len_old_end_tags],
                  tmp_end_tags, len_old_end_tags);
          memset(tmp_end_tags, 0, len_old_end_tags);
        }
      char* tmp_char = malloc(len_end_tags);
      strcpy(tmp_char, "</");
      strncat(tmp_char, tmp_func->name, strlen(tmp_func->name));
      strncat(tmp_char, ">", 2);
      memcpy(tmp_end_tags, tmp_char, strlen(tmp_char));
      free(tmp_char);
      tmp_func = next_function(func_lst, tmp_func);
    }
  *begin_tags = strndup(tmp_begin_tags, len_begin_tags);
  *end_tags = strndup(tmp_end_tags, len_end_tags);
  free(tmp_begin_tags);
  free(tmp_end_tags);
}


void convert_single_section_to_html(section *sec)
/* converts a section into html - non-recursive */
{
  if (sec == NULL) return;
  char *begin_tags = NULL, *end_tags = NULL;
  char* output_str;
  unsigned long int len_output_str;
  make_html_tags_from_function_list(sec->functions,
  				    &begin_tags,
  				    &end_tags);
  len_output_str = strlen(begin_tags)
    + strlen(end_tags)
    + sec->blength + 1;
  output_str = malloc(len_output_str);
  memset(output_str, 0, len_output_str);
  strncat(output_str, begin_tags, strlen(begin_tags));
  strncat(output_str, sec->body, sec->blength);
  strncat(output_str, end_tags, strlen(end_tags));
  free(begin_tags);
  free(end_tags);
  free(sec->body);
  sec->body = output_str;
  sec->blength = strlen(sec->body);
}

int convert_section_to_html(section *sec)
{
  if (sec == NULL)
    return 0;
  convert_single_section_to_html(sec);
  add_body_to_parent(sec);
  return 0;
}


void add_body_to_parent(section* sec)
{
  if (sec->parent)
    {
      char* parent_body =
        merge_into_body(sec->body,
                        sec->parent->body,
                        sec->parent->body_cursor + sec->offset);
      free(sec->parent->body);
      sec->parent->body = parent_body;
      sec->parent->blength = strlen(parent_body);
      sec->parent->body_cursor += sec->blength + sec->offset;
    }
}

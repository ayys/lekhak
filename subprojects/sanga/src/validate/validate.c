/*
   this is a little module that takes in a string
   with sanga code and validates it.

   The validation process does teh following:

   1. Check if the number of [ parens match the number of
   ] parens

   2. Check if the number of { parens match the number of
   } parens
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <sanga/validate.h>

int validate_sanga_text(char* sanga_text, unsigned long int text_size)
{
  /* this function takes sanga_text, and size. Then loops over
     every character to check if the sanga text is valid.
     Returns: 0 if matches, -1 if No. of square braces is not 0,
     -2 is no. o fcurly braces is not 0.
   */

  /* no_sq_braces: Number of square braces
     no_crl_braces: Number of curly braces
   */
  unsigned long int no_sq_braces = 0,
    no_crl_braces = 0;

  unsigned int return_flag = 0;

  for (int i = 0; i < text_size; i++)
    {
      switch (sanga_text[i])
        {
        case '\\':
          i++;
          break;
        case '[':
          no_sq_braces++;
          break;
        case ']':
          no_sq_braces--;
          break;
        case '{':
          no_crl_braces++;
          break;
        case '}':
          no_crl_braces--;
          break;
        default:
          break;
        }
    }
  if (no_sq_braces != 0)
    {
      return_flag = return_flag | SQUARE_BRACE_MISMATCH;
    }
  if (no_crl_braces != 0)
    {
      return_flag = return_flag |  CURLY_BRACE_MISMATCH;
    }
  return return_flag;
}


int validate_sanga(FILE* sanga_file)
{
  struct stat file_stat;
  fstat(fileno(sanga_file),
        &file_stat);
  off_t file_size = file_stat.st_size;
  char* sanga_text = malloc(file_size + 1);
  fread(sanga_text, file_size, 1, sanga_file);
  sanga_text[file_size] = '\0';
  int return_flag = validate_sanga_text(sanga_text, file_size);
  free(sanga_text);
  return return_flag;
}

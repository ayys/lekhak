/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/




#include <sanga/section.h>


#ifndef DOM_H

#define DOM_H
/**
   \struct DOM dom.h
   This structure represents the Document Object  Model
   for Sanga. The DOM takes care of the syntax tree and other
   book-keeping tasks such as keeping track of the number of
   sections that are in the sytnax tree.

   There can only be one DOM Object in the whole program.
 */
struct DOM
{
  /**
     This is the pointer to top level section. The address is stored
     in the DOM Object.
  */
  section *root;
  /**
     This unsigned int stores the number of sections that are currently
     available in the whole syntax tree.
   */
  unsigned long nsections;
};

/**
   Since there can only be one struct DOM object in the whole program,
   an extern is provided for that reason. All libraries needing to access
   the DOM Object must declare this in their sources.
*/
extern struct DOM *dom;

/**
   This function initializes the DOM Object. Any module trying to
   initialize the DOM Object must use this function.
   The DOM Object may only be initialized once. More than one intialization
   may cause undefined behaviour.
 */
struct DOM* initialize_DOM();

#endif

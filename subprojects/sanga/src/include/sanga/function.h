/*
  \author Ayush Jha
  This file is part of Sanga.
           
  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
           
  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
           
  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.   
*/
           



#ifndef FUNCTION_H
#define FUNCTION_H


/**
   \brief loop over each function in the function list
   and apply the function

   @param[in] func_l function list is of type function_list*
   @param[in] function  pointer to a function with one argument, struct function*

   for_each_function takes a function list, and a function pointer,
   and applies the function to all the functions in the function list.
   The loop starts from the left most function defined in the function list and 
   moves to the right.
   So, for example,
   \verbatim [{bold italics} body of the section] \endverbatim
   First, the `bold` function will be applied to the function before the italics
   function.
*/
#define for_each_function(func_l, function) do{                         \
    int return_val;                                                     \
    for(unsigned int ctr = 0; ctr < *(func_l->nfunctions); ctr++)       \
      {                                                                 \
        return_val = function(func_l->functions[ctr]);                  \
        if (return_val != 0)                                            \
          break;                                                        \
      }                                                                 \
  } while (0)


/**
   \struct function function.h
   @brief stores the information about a single function 
 */
struct function
{
  char* name;                   /**< name of the function  */
  char* argument;               /**< argument of the function  */
  unsigned int ctr;             /**< nth function in the function list */
};

/***
   @brif Struct stores information about the functions
*/
typedef struct function_list
{
  unsigned int nfunctions; 	  /**< Number of functions currently being used */
  unsigned int max_nfunctions;    /**< Maximum number of functions available */
  struct function *functions;     /**< dynamic array of functions of size max_nfunctions */
} function_list;


/**
   \brief creates a function_list struct with given nfunctions
   \param[in] nfunctions Number of functions that the function_list should have
   
   Creates a function list with nfunctions number of functions
   the functions are memst to be 0.
*/
function_list* create_function_list(unsigned int nfunctions);


/**
   \brief adds a function to the function_list
   function.h with given name and argument 

   \param[out] func_l  to which function is added
   \param[in] name  Name of the newly created function
   \param[in] argument Argument of the newly created function

   Adds a new function with the specified name and argument to the
   function_list
*/
void add_function(function_list * func_l, char* name, char * argument);

/**
   \brief Free the given function_list
   \param[in] func_l The function list  which is to be freed
   frees the function_list, it's containing functions, their names and arguments
   All the memory allocated to a function list is freed here.
*/
void free_function_list(function_list *func_l);

/**
   \param[in]  func_l function_listfrom which the next function is taken
   \param[in]  func function whose next function is needed

   Takes a function list, and a function. The function has a ctr value.
   This function returns the next function, i.e. the function with ctr value one
   greater than that of the current function. If no function is found, NULL is
   returned.
 */
struct function* next_function(function_list* func_l, struct function* func);


#endif

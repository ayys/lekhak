/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STRUCTURE_SIZES
#define STRUCTURE_SIZES
#include <libguile.h>
/**
   10KB LIMIT ON BODY SIZE OF SECTION
*/
#define BODY_SIZE 10240

/**
   This is a macro to create a new char* ptr of specified body size
 */
#define CREATE_NEW_BODY scm_malloc(BODY_SIZE)

/**
   This is a macro used to create an empry section.
 */
#define CREATE_NEW_SECTION scm_gc_malloc(sizeof(section), "section")

/**
   The default Maximum length of a function name
 */
#define SECTION_NAME_LENGTH 100
#endif


#ifndef SECTION_STRUCT
#define SECTION_STRUCT

#include <sanga/function.h>


struct section
{
  /**
     NAME OF THE SECTION */
  struct function_list *functions;
  /**
     BODY OF THE SECTION */
  char *body;
  /**
     offset IS USED BY THE PARENT SECTION TO DETERMINE WHEN THE CHILD SECTION BEGINS */
  unsigned long offset;
  /**
     USED TO KEEP TRACK OF THE POSITION OF CURSOR WHEN PLACING CHILDREN
     BODY INTO THE PARENT
  */
  unsigned long body_cursor;
  /**
     LENGTH OF THE BODY */
  unsigned long blength;
  /**
     THE PARENT SECTION IS LINKED VIA THIS PROPERTY
  */
  struct section *parent;
  /**
     SIBLING SECTION, USED TO IMPLEMENT THE LINKED LIST OF CHILDREN
     NUMBER OF SIBLINGS IS GIVEN BY THE PARENT VIA THE NCHILDREN
     PROPERTY. WHEN THE SECTION DOESN'T HAVE ANY MORE SIBLINGS,
     next_sibling IS SET TO NULL
  */
  struct section *next_sibling;
  /**
     LINKED LIST OF POINTERS TO IT'S CHILDREN
     THE POINTER first_child CONTAINS A CHILD.
  */
  struct section *first_child;
  /**
     NUMBER OF CHILDREN.
     nclildren MUST BE INCREMENTED EVERYTIME A NEW CHILD IS ADDED
  */
  unsigned long nchildren;
};

#endif

#ifndef SECTION_CODE
#define SECTION_CODE

typedef struct section section;

/**
   This function frees the given section, and it's children.
   This function also free's all the character arrays created
   for the body, and the function list linked to the section.
   This function internally calls free_function_list

   \param[in] sec section of DOM from which to free the sections.eg. (dom->root)
*/
void free_section(section* sec);

/**
   This function returns the last immediate child section
   of a given section sec. The immediate child is the set of
   siblings for the first_child of the section.

   \param[in] sec The parent section for which the last child is needed
 */
section* section_get_last_child(section* sec);
/**
   This function adds a new child to the parent section.
   A child is always added to the end of the linked list.
   So, this function takes the get_last child and links the
   child to the last child of the parent
   Both parent and child need to be dynamically allocated.

   \param[in,out] parent is the parent section which the child is added
   \param[in] child is the child section which is to be added to the parent
 */
int section_add_child(section* parent, section* child);

/**
   Creates a new section and returns the pointer.
   Along with the section, it also creates and links
   the funciton list to the section, and memsets the whole
   section to 0. Also, it uses the buffer_length set by
   lexer.l to set the initial buffer length for the section body.
 */
section* create_section();

/**
   Update the new section with a body and parent.

   \param[in,out] new section to which the body and parent are set
   \param[in] body The body of the section.
   \param[in] parent The parent section which is set to the new section
 */
section* update_section(section* new, char* body, section* parent);

/**
   Apply the function called function to all the sections, starting from
   the section called sec, and it's children. The function is applied to
   all the children before it is applied to the parent.
   The funtion is not applied to the parent of section sec.

   \param[in] sec Section from which to start applying the function
   \param[in] function Function which is to be applied to the sections
 */
int apply_to_each_section(section* sec, int (*function)(section*));

/**
   parent_sec is used to define the parent_sec of the current section.
   This is used by parser.y
 */
extern section *parent_sec, *current_sec/** current_sec is used to define the current section. This is used by parser.y. */;
/**
   The length of body buffer as set by the lexer.
   This is used to create sections using the create_section function
 */
extern int buffer_length;

#endif

#ifndef SECTION_API_H
#define SECTION_API_H

#endif

/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/




/* header file for print.c */

#include <sanga/dom.h>

#ifndef LANG_H
#define LANG_H

/* lower level functions : DO NOT USE THESE  */
int parse_sanga(char* filename);
void free_ast();

/* higher level functions, please use these only */
struct DOM *parse(char* filename);
void free_AST(struct DOM *dom);

#endif

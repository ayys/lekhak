/*
  \author Ayush Jha
  This file is part of Sanga.
           
  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
           
  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
           
  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.   
*/
           



/* header file for print.c */

#ifndef PRINT_H
#define PRINT_H

/**
   This function prints the section, with all it's properties
   and it recursively print's the children section as well.
   \param[in] section Takes the root section as input
 */
void print_section (section* section);

/**
   WELCOME_MESSAGE is the heading that is printed if the
   sanga interpreter is run in interactive mode.
 */
#define WELCOME_MESSAGE "\nWELCOME TO SANGA 0.0.1-alpha\n"      \
  "LICENCED UNDER GNU GPL 3.0\n\n"                              \

/**
   INTERPRETER_HEAD is the heading that is shown before each input. 
 */
#define INTERPRETER_HEAD "SANGA> "
#endif

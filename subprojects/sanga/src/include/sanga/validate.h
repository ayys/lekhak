#ifndef VALIDATE_H

#define VALIDATE_H

#define CURLY_BRACE_MISMATCH 1
#define SQUARE_BRACE_MISMATCH 2


int validate_sanga_text(char* sanga_text, unsigned long int text_size);
int validate_sanga(FILE* sanga_file);

#endif

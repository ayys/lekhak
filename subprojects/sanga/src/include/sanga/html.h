/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/




#include <sanga/section.h>

#ifndef HTML_H
#define HTML_H
/**
   Takes a section and converts it's body into HTML.
   This function is non-recursive, meaning it only
   works on a single section.
   This function may only be called once per section.
   Multiple calls over the same section produces undefined
   behaviour.

   \param[in] sec Section to convert to HTML
 */
void convert_single_section_to_html(section *sec);
/**
   This function takes a section, and places it's body
   into it's parent's body at the offset specified by
   sec->offset.

   \param[in] sec Child section, whose body is placed in it's parents.
 */
void add_body_to_parent(section*sec);
/**
   Takes a section and converts it's body into HTML.
   This function is recursive, and it internally calls
   convert_single_section_to_html. This function takes
   a section and converts it, along with all it's children
   to HTMLL, and then adds the body to form the total HTML
   document.

   \param[in] sec The root section from which to start the conversion
 */
int convert_section_to_html(section *sec);

#endif

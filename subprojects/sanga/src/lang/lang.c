/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <libguile.h>

#include <sanga/parser.tab.h>
#include <sanga/lex.yy.h>
#include <sanga/section.h>
#include <sanga/dom.h>
#include <sanga/lang.h>

int parse_sanga(char* filename)
{
  int status;
  FILE* in_file;
  YY_BUFFER_STATE bp;
  in_file = fopen(filename, "r");
  bp = yy_create_buffer(in_file, YY_BUF_SIZE);
  yy_switch_to_buffer(bp);
  status = yyparse(); /* yyparse PARSES ONE STATEMENT OF INPUT AT A TIME*/
  /* 
     these are variables used by the parser to store state.
     these need to be set to NULL after parsing is done.
 */
  yy_delete_buffer(bp);
  parent_sec = NULL;
  current_sec = NULL;
  if (status != 0)
    return -1;
  return 0;
}

void free_ast()
{
  free_section(dom->root);
  yylex_destroy();
}


struct DOM *parse(char* filename){
  yyin = fopen(filename, "r");
  struct DOM *d = initialize_DOM();
  parse_sanga(filename);
  fclose(yyin);
  return d;
}

void free_AST(struct DOM *d){
  dom = d;
  free_ast();
}

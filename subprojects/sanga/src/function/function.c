/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/




#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <libguile.h>

#include <sanga/function.h>

#define FUNCTIONS_INC_AMOUNT 10
#define MULTIPLY(A,B) ((A) * (B))


function_list* create_function_list(unsigned int nfunctions)
{
  function_list* func_l = { 0 };
  func_l = scm_gc_malloc(sizeof(function_list), "function-list");
  memset(func_l, 0, sizeof(function_list));
  func_l->max_nfunctions = nfunctions;
  func_l->nfunctions = 0;
  func_l->functions = scm_gc_malloc(sizeof(struct function) * nfunctions, "funtion");
  return func_l;
}


void add_function(function_list *func_l,
                  char* function_name,
                  char* function_argument)
/*
  adds a new function to the function list func_l
  with a function name and function arguments
*/
{
  /*
    Since functions uses dynamic array to store the components,
    this is the logic.
    if (fl->nfunctions >= fl->max_nfunctions)
    reallocate fl->functions
    function = fl->functions[++nfunctions]
    set the function properties

    NOTE: For further information about fl,
    read struct function_list in function.h
  */
  if (func_l->nfunctions >= func_l->max_nfunctions)
    {
      func_l->functions = scm_gc_realloc(func_l->functions,
                                         func_l->max_nfunctions,
                                         /*
                                           using MULTIPLY to make the code fit in 80 characters/line
                                           because using "*" does not provide good indentation
                                           and sometimes, the indentation leaves too little space
                                           for the actual code
                                         */
                                         MULTIPLY(
                                                  sizeof(struct function),
                                                  (func_l->max_nfunctions + FUNCTIONS_INC_AMOUNT)),
                                         "function"
                                         );
      func_l->max_nfunctions += FUNCTIONS_INC_AMOUNT;
    }
  func_l->functions[func_l->nfunctions].name = function_name;
  func_l->functions[func_l->nfunctions].argument = function_argument;
  func_l->functions[func_l->nfunctions].ctr = func_l->nfunctions;
  func_l->nfunctions++;
}

void free_function_list(function_list *func_l)
{
  if (func_l == NULL)
    return;
  for(int i = 0; i < func_l->nfunctions; i++)
    {
      free(func_l->functions[i].name);
      free(func_l->functions[i].argument);
    }
  scm_gc_free(func_l->functions, sizeof(struct function), "function");
  scm_gc_free(func_l, sizeof(function_list), "function-list");
}


struct function* next_function(function_list* func_l,
                               struct function *current_func)
{
  if (current_func->ctr + 1 >= func_l->nfunctions)
    return NULL;
  return &(func_l->functions[current_func->ctr + 1]);
}

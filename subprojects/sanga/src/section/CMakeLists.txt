cmake_minimum_required(VERSION 3.0)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
find_package(Guile REQUIRED)

add_library(
  sangasection SHARED
  section.c
  )

target_link_libraries( sangasection ${GUILE_LIBRARIES} sangafunction)

install(TARGETS sangasection DESTINATION /usr/lib)

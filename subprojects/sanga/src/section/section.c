/*              \author Ayush Jha
               This file is part of Sanga.

               Sanga is free software: you can redistribute it and/or modify
               it under the terms of the GNU General Public License as published by
               the Free Software Foundation, either version 3 of the License, or
               (at your option) any later version.

               Sanga is distributed in the hope that it will be useful,
               but WITHOUT ANY WARRANTY; without even the implied warranty of
               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
               GNU General Public License for more details.

               You should have received a copy of the GNU General Public License
               along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
           */




/*
  This file contains the data structures used with
  sanga along wiht their respective methods
*/

#include <stdlib.h>
#include <string.h>
#include <libguile.h>

#include <sanga/section.h>
#include <sanga/function.h>



/* Variable definition */
section* root_sec = NULL;
section *parent_sec, *current_sec = NULL;
int buffer_length = 0;		/* buffer length set by flex */

int __apply_to_each_section(struct section *sec,
                             int (*each_function)(section *),
                             int force_current,
                             unsigned int nth_sibling)
{
  struct section* child_section;

  if (sec == NULL)
    return 0;

  if (force_current == 1)
    {
      child_section = sec;
    }
  else
    {
      child_section = sec->first_child;
    }
  if (child_section == NULL || force_current == 1){
    if (each_function != NULL)
      {
        int ret_val = each_function(sec);
        if (ret_val != 0)
          {
            return -1;
            /* unsuccessful return */
          }
      }
    if (sec->next_sibling != NULL)
      {
	__apply_to_each_section(sec->next_sibling,
                                each_function, 0, nth_sibling+1);
      }
    else if (sec->parent != NULL)
      {
	__apply_to_each_section(sec->parent, each_function, 1, 0);
      }
    else
      return 0;
  }
  else
    {
      __apply_to_each_section(child_section, each_function, 0, 1);
    }
  return 0;                     /* successful return */
}


int apply_to_each_section(struct section *sec, int (*function)(section *))
{
  return __apply_to_each_section(sec, function, 0, 1);
}


void free_section(struct section *sec)
{
  struct section* tmp;
  if (sec == NULL)
    return;

  free_function_list(sec->functions);
  tmp = sec->first_child;

  free(sec->body);
  scm_gc_free(sec, sizeof(section), "section");

  while (tmp != NULL)
    {
      section * s = tmp;
      tmp = tmp->next_sibling;
      free_section (s);
    }
}


section* section_get_last_child (section *root)
{
  if (root->first_child == NULL)
    {
      /* no children found */
      return NULL;
    }
  else
    {
      /* there are one or more children */
      section *child_ptr;
      child_ptr = root->first_child;
      while(child_ptr->next_sibling != NULL)
	{
	  child_ptr = child_ptr->next_sibling;
	}
      return child_ptr;
    }
}

int section_add_child(section *parent, section *child)
{
  child->parent = parent;
  if (parent->first_child == NULL)
    {
      parent->first_child = child;
      parent->nchildren = 1;
    }
  else
    {
      section *tmp;
      tmp = section_get_last_child (parent);
      tmp->next_sibling = child;
      parent->nchildren++;
    }
  return 0;
}

section* create_section()
{
  section *new = CREATE_NEW_SECTION;
  memset(new, 0, sizeof(section));
  new->functions = create_function_list(1);
  new->offset = buffer_length;
  new->body_cursor = 0;
  return new;
}

section* update_section(
			section *new,
			char* body,
			section* parent)
{
  new->body = body;
  if (new->parent == NULL)
    new->parent = parent;
  if (new->body != NULL)
    new->blength += strlen(new->body);
  return new;
}

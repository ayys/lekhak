(define-module (sanga html)
  #:export (sanga->sxml-bold))

(use-modules (sanga lang))

;; रुपान्तरण कार्य
(define (sanga->sxml-bold s)
  `(b ,(s:body s) ))

(define-module (sanga lang)
  #:export (s:functions
            s:children
            s:children?
            s:next-sibling?
            s:section-name
            s:fn-with-name
            s:parent?
            s:has-function?
            s:section-list
            s:free-asts
            s:merge-with-children
            s:merge-with-children!
            s:parse))

(load-extension "libsangaast" "scm_init_sanga_lang_module")

(use-modules (sanga validate))

(define (__s:guilize-function function)
  (list (s:function-name function)
        (s:function-argument function)))


(define (s:next-sibling? s)
  (if (s:next-sibling s)
      #t
      #f))

(define (s:children? section)
  (if (> (s:nchildren section) 0)
      #t
      #f))

(define (s:parent? section)
  (if (s:parent section)
      #t
      #f))


(define (__s:functions section funcs nfuncs)
  "returns a list of functions for the section"
  (if (> nfuncs 0)
      (__s:functions section
                     (cons (s:function section (1- nfuncs))
                           funcs)
                     (1- nfuncs))
      (map __s:guilize-function funcs)))


(define (s:functions section)
  "returns a list of functions for the section"
  (__s:functions section '() (s:nfunctions section)))


(define (__s:children child children)
  (if (s:next-sibling? child)
      (__s:children (s:next-sibling child)
                    (cons child children))
      (cons child children)))

(define (s:children parent-section)
  "returns a list of children for the section"
  (if (not (zero? (s:nchildren parent-section)))
      (__s:children (s:first-child parent-section)
                    '())))


(define (s:parse filename)
  (define dom-obj (s:parse-file filename))
  (if (s:validate-functions (s:root dom-obj))
      dom-obj
      #f))

(define (__s:has-function? function-name section current-function-index)
  (if (>= current-function-index 0)
      (begin
        (if (eq? function-name
                 (s:function-name (s:function section
                                              current-function-index)))
            #t
            (__s:has-function? function-name section
                               (1- current-function-index))))
      #f))

(define (s:has-function? function-name section)
  (__s:has-function? function-name section (1- (s:nfunctions section))))

(define (__s:merge-with-children parent children tmp cursor)
  "initially, tmp = (s:body parent), and cursor = 0"
  (define (string-merge-at sm sn off)
    "merge sn into sm at offset = off"
    (display off)(newline)
    (display (string-length sm))(newline)
    (string-join (list
                  (string-take sm off)
                  sn
                  (string-drop sm off))
                 ""))
  (if (null? children)
      tmp
      (__s:merge-with-children
       parent
       (cdr children)
       (string-merge-at tmp
                        (s:body (car children))
                        (+ cursor (s:offset (car children))))
       (+ cursor (s:offset (car children))
          (string-length (s:body (car children)))))))

(define (s:merge-with-children s)
  "merge all it's childen's body into it's own and return mergeed body"
  (if (s:children? s)
      (__s:merge-with-children
       s
       (reverse (s:children s))
       (s:body s)
       0)
      (s:body s)))

(define (s:merge-with-children! s)
  "same as s:merge-with-children but will set <section>s's body"
  (s:set-body! s (s:merge-with-children s)))


(define (__s:section-list current-section slst descend)
  (if current-section
      (if descend
          (if (s:children? current-section)
              (__s:section-list (s:first-child current-section)
                                slst descend)
              (if (s:next-sibling? current-section)
                  (__s:section-list (s:next-sibling current-section)
                                    (cons current-section slst)
                                    #t)
                  (__s:section-list (s:parent current-section)
                                    (cons current-section slst)
                                    #f)))
          (if (s:next-sibling? current-section)
              (__s:section-list (s:next-sibling current-section)
                                (cons current-section slst)
                                #t)
              (__s:section-list (s:parent current-section)
                                (cons current-section slst)
                                #f)))
      slst))

(define (s:section-list s)
  (__s:section-list s '() #t))

(define (s:free-asts asts)
  (apply (lambda (x y) (and x y))
         (map s:free-ast asts)))

(define (s:section-name s)
  (if s  (caar (s:functions s))))

(define (s:fn-with-name s name)
  (let ((शीर्षक (filter (lambda (f) (equal? (car f) name)) (s:functions s))))
    (if (> (length शीर्षक) 0)
        (car शीर्षक)
        #f))
  )

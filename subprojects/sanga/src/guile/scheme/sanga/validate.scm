(define-module (sanga validate)
  #:export (s:validate-functions))

(use-modules (sanga lang))
(use-modules (srfi srfi-1))

(define valid-function-names
  '(
    ("लेख" "शीर्षक" "उपशीर्षक" "लेखकहरू" "प्रकाशकको नाम" "प्रकाशकको ठेगाना"
     "प्रकाशनको मिति" "संस्करण" "श्रीङ्खला" "असिकिमु" "विषय")
    ("पाठ" "शीर्षक" "उपशीर्षक" "लेखकहरू" "प्रकाशकको नाम" "प्रकाशकको ठेगाना"
     "प्रकाशनको मिति" "विषय" )
    ("खण्ड" "नाम")
    ("उपखण्ड" "नाम")
    ("गाढो")
    ("छर्के")
    ("अक्षरुप")
    ("सुची" "प्रत्यय" "उपसर्ग")
    ("तालिका")
    ))

(define (s:validate-functions s)
  ;; check if the functions are valid for given section s
  ;; Is the main function(first function) valid?
  ;; Do all the other functions adhire to the rules of main function?
  (let ((funcs (s:functions s)))
    (define func-lst (find (lambda (x)
                             (string=? (car x) (caar funcs)))
                           valid-function-names))
    (if (every (lambda (x)
                 (member (car x) func-lst))
               funcs)
        #t #f)))

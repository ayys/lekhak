/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <sanga/dom.h>
#include <sanga/lang.h>
#include <sanga/section.h>

#include <libguile.h>

static SCM section_type,
  function_type,
  dom_type,
  function_list_type;

void
init_section_type ()
{
  SCM name, slots;
  scm_t_struct_finalize finalizer;
  name = scm_from_utf8_symbol("section");
  slots = scm_list_1(scm_from_utf8_symbol("data"));
  finalizer = NULL;

  section_type =
    scm_make_foreign_object_type(name, slots, finalizer);
}

void
init_dom_type ()
{
  SCM name, slots;
  scm_t_struct_finalize finalizer;
  name = scm_from_utf8_symbol("dom");
  slots = scm_list_1(scm_from_utf8_symbol("data"));
  finalizer = NULL;

  dom_type =
    scm_make_foreign_object_type(name, slots, finalizer);
}

void
init_function_type ()
{
  SCM name, slots;
  scm_t_struct_finalize finalizer;
  name = scm_from_utf8_symbol("function");
  slots = scm_list_1(scm_from_utf8_symbol("data"));
  finalizer = NULL;

  function_type =
    scm_make_foreign_object_type(name, slots, finalizer);
}

void
init_function_list_type ()
{
  SCM name, slots;
  scm_t_struct_finalize finalizer;
  name = scm_from_utf8_symbol("function-list");
  slots = scm_list_1(scm_from_utf8_symbol("data"));
  finalizer = NULL;

  function_list_type =
    scm_make_foreign_object_type(name, slots, finalizer);
}


SCM get_root(SCM dom_obj)
{
  scm_assert_foreign_object_type (dom_type, dom_obj);
  struct DOM* d = scm_foreign_object_ref(dom_obj, 0);
  return scm_make_foreign_object_1(section_type, d->root);
}

SCM get_parent(SCM section_obj)
{
  section *sec;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  if (sec->parent != NULL)
    return scm_make_foreign_object_1(section_type, sec->parent);
  return SCM_BOOL_F;
}


SCM get_section_function_list(SCM section_obj)
{
  section *sec;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  return scm_make_foreign_object_1(function_list_type, sec->functions);
}

SCM get_section_function(SCM section_obj, SCM func_index)
{
  section *sec;
  int index;
  index = scm_to_int(func_index);

  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  return scm_make_foreign_object_1(function_type,
                                   &(sec->functions->functions[index]));
}

SCM get_section_function_name(SCM function_obj)
{
  struct function *func;

  scm_assert_foreign_object_type (function_type, function_obj);
  func = scm_foreign_object_ref(function_obj, 0);
  return scm_from_utf8_string(func->name);
}

SCM get_section_function_argument(SCM function_obj)
{
  struct function *func;

  scm_assert_foreign_object_type (function_type, function_obj);
  func = scm_foreign_object_ref(function_obj, 0);
  if (func->argument == NULL)
    return SCM_BOOL_F;
  return scm_from_utf8_string(func->argument);
}


SCM get_section_body(SCM section_obj)
{
  section *sec;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  return scm_from_utf8_string(sec->body);
}

SCM set_section_body(SCM section_obj, SCM sbody)
{
  section *sec;
  char* body;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);

  body = scm_to_utf8_string(sbody);
  free(sec->body);
  sec->body = body;
  return SCM_BOOL_T;
}

SCM get_section_offset(SCM section_obj)
{
  section *sec;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  return scm_from_ulong(sec->offset);
}

SCM get_section_nchildren(SCM section_obj)
{
  section *sec;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  return scm_from_ulong(sec->nchildren);
}

SCM get_section_nfunctions(SCM section_obj)
{
  section *sec;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  return scm_from_ulong(sec->functions->nfunctions);
}


SCM get_section_next_sibling(SCM section_obj)
{
  section *sec;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  if (sec->next_sibling == NULL)
    return SCM_BOOL_F;
  return scm_make_foreign_object_1(section_type,
                                   sec->next_sibling);
}

SCM get_section_first_child(SCM section_obj)
{
  section *sec;
  scm_assert_foreign_object_type (section_type, section_obj);
  sec = scm_foreign_object_ref(section_obj, 0);
  if (sec->first_child == NULL)
    return SCM_BOOL_F;
  return scm_make_foreign_object_1(section_type,
                                   sec->first_child);
}

SCM free_ast_guile (SCM dom_obj)
{
  scm_assert_foreign_object_type (dom_type, dom_obj);
  struct DOM* d = scm_foreign_object_ref(dom_obj, 0);
  free_AST(d);
  return SCM_BOOL_T;
}

SCM parse_file(SCM filename)
{
  char* c_filename = scm_to_utf8_string(filename);
  struct DOM* d = parse(c_filename);
  return scm_make_foreign_object_1(dom_type, d);
}

SCM is_section_equal (SCM s1, SCM s2)
{
  scm_assert_foreign_object_type (section_type, s1);
  scm_assert_foreign_object_type (section_type, s2);
  section *sec1, *sec2;

  sec1 = scm_foreign_object_ref(s1, 0);
  sec2 = scm_foreign_object_ref(s2, 0);
  if (sec1 == sec2)
    return SCM_BOOL_T;
  return SCM_BOOL_F;
}

void init_sanga_lang()
{
  init_section_type();
  init_dom_type();
  init_function_type();
  init_function_list_type();
  scm_c_define_gsubr("s:parse-file", 1, 0, 0, parse_file);
  scm_c_define_gsubr("s:free-ast", 1, 0, 0, free_ast_guile);
  scm_c_define_gsubr("s:root", 1, 0, 0, get_root);
  scm_c_define_gsubr("s:parent", 1, 0, 0, get_parent);
  scm_c_define_gsubr("s:body", 1, 0, 0, get_section_body);
  scm_c_define_gsubr("s:set-body!", 2, 0, 0, set_section_body);
  scm_c_define_gsubr("s:offset", 1, 0, 0, get_section_offset);
  scm_c_define_gsubr("s:nchildren", 1, 0, 0, get_section_nchildren);
  scm_c_define_gsubr("s:next-sibling", 1, 0, 0, get_section_next_sibling);
  scm_c_define_gsubr("s:first-child", 1, 0, 0, get_section_first_child);
  scm_c_define_gsubr("s:nfunctions", 1, 0, 0, get_section_nfunctions);
  scm_c_define_gsubr("s:function-list", 1, 0, 0,
                     get_section_function_list);
  scm_c_define_gsubr("s:function", 2, 0, 0,
                     get_section_function);
  scm_c_define_gsubr("s:function-name", 1, 0, 0,
                     get_section_function_name);
  scm_c_define_gsubr("s:function-argument", 1, 0, 0,
                     get_section_function_argument);

  scm_c_define_gsubr("s:section-equal?", 2, 0, 0,
                     is_section_equal);


  scm_c_export("s:parse-file",
               "s:free-ast",
               "s:root",
               "s:parent",
               "s:body",
               "s:set-body!",
               "s:offset",
               "s:nchildren",
               "s:next-sibling",
               "s:first-child",
               "s:nfunctions",
               "s:function-list",
               "s:function",
               "s:function-name",
               "s:function-argument",
               "s:section-equal?",
               NULL);
}


void scm_init_sanga_lang_module()
{
  scm_c_define_module ("sanga lang", init_sanga_lang, NULL);
}

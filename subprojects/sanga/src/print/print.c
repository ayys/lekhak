/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/




/*
  Printing function for the section AST
*/
#include <stdio.h>
#include <sanga/section.h>
#include <sanga/function.h>

int print_function(struct function* func)
{
  printf("\tFUNCTION NAME: %s\t ARGUMENT: %s\n",
         func->name,
         func->argument);
  return 0;
}

void print_section(section *sec)
{
  /* FIRST, PRINT THE TOP LEVEL STUFF */
  if (sec == NULL)
    return;
  printf("SECTION -- %p\n", sec);
  printf("SECTION NAMES:\n");
  for_each_function(&(sec->functions), print_function);
  printf("BODY LENGTH: %lu\n", sec->blength);
  printf("BODY OFFSET: %lu\n", sec->offset);
  printf("BODY: %s\n", sec->body);
  printf("NUMBER OF CHILDREN: %lu\n", sec->nchildren);
  printf("FIRST_CHILD: %p\n", sec->first_child);
  printf("NEXT SIBLING: %p\n", sec->next_sibling);
  printf("\n-------------------\n");
  struct section* tmp = sec->first_child;
  while (tmp != NULL)
    {
      print_section (tmp);
      tmp = tmp->next_sibling;
    }

}

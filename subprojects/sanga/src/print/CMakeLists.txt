cmake_minimum_required(VERSION 2.8)

add_library(
  sangaprint SHARED
  print.c
  )
install(TARGETS sangaprint DESTINATION /usr/lib)

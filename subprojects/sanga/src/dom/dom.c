/*
  \author Ayush Jha
  This file is part of Sanga.

  Sanga is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Sanga is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sanga.  If not, see <https://www.gnu.org/licenses/>.
*/




#include <stdlib.h>

#include <sanga/section.h>
#include <sanga/dom.h>

struct DOM *dom = NULL;

struct DOM* initialize_DOM()
{
  struct DOM *tmp_dom = malloc(sizeof(struct DOM));
  tmp_dom->root = malloc(sizeof(section));
  tmp_dom->nsections = 1;
  dom = tmp_dom;
  return dom;
}

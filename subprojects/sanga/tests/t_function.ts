#include <stdlib.h>
#include <sanga/function.h>

#suite Function

function_list *func_l;
char *name, *argument;

void free_func_l()
{
  free_function_list(func_l);
}

void setup_add_function()
{
  func_l = create_function_list(1);
  name = malloc(5);
  strcpy(name, "Hello");
  argument = malloc(5);
  strcpy(argument, "Hello");
}

void teardown_add_function()
{
  free_func_l();
}

#tcase Create Function List

#test test_create_function_list
func_l = create_function_list(1);
ck_assert_int_eq(func_l->nfunctions, 0);
ck_assert_int_eq(func_l->max_nfunctions, 1);
ck_assert(func_l->functions);


#tcase Add Function

#test test_add_function_no_args
add_function(func_l, name, NULL);
ck_assert_int_eq(func_l->nfunctions, 1);
ck_assert_ptr_nonnull(func_l->functions[0].name);
ck_assert_str_eq(func_l->functions[0].name, "Hello");
ck_assert_ptr_null(func_l->functions[0].argument);

#test test_add_function_with_args
add_function(func_l, name, argument);
ck_assert_int_eq(func_l->nfunctions, 1);
ck_assert_ptr_nonnull(func_l->functions[0].name);
ck_assert_str_eq(func_l->functions[0].name, "Hello");
ck_assert_ptr_nonnull(func_l->functions[0].argument);
ck_assert_str_eq(func_l->functions[0].argument, "Hello");


#main-pre
tcase_add_checked_fixture(tc1_2, setup_add_function, teardown_add_function);

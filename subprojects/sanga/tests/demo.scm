(use-modules (sanga lang))

(define dom (s:parse "test.sanga"))
(define dom-other (s:parse "test1.sanga"))

(define (print-dom d)
  (if d (begin
          (display (s:functions (s:root d)))
          (newline))))

(map print-dom
     (list dom dom-other))

(display "Freed all DOMs? : ")
(display (s:free-asts (list dom dom-other)))
(newline)

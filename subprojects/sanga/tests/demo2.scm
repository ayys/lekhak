(use-modules (sanga lang))

(define dom (s:parse "test.sanga"))
(define dom-other (s:parse "test1.sanga"))

(define (get-fun-arg fn)
  (if fn
      (cadr fn)
      #f))

(define (s:section-to-lisp s)
  `(लेख
    #:शीर्षक
    ,(get-fun-arg (s:fn-with-name s "शीर्षक"))
    #:उपशीर्षक
    ,(get-fun-arg (s:fn-with-name s "उपशीर्षक"))
    #:लेखकहरू
    ,(get-fun-arg (s:fn-with-name s "लेखकहरू"))
    #:प्रकाशकको_नाम
    ,(get-fun-arg (s:fn-with-name s "प्रकाशकको नाम"))
    #:प्रकाशकको_ठेगाना
    ,(get-fun-arg (s:fn-with-name s "प्रकाशकको ठेगाना"))
    #:प्रकाशनको_मिति 
    ,(get-fun-arg (s:fn-with-name s "प्रकाशनको मिति"))
    #:संस्करण 
    ,(get-fun-arg (s:fn-with-name s "संस्करण"))
    #:श्रीङ्खला 
    ,(get-fun-arg (s:fn-with-name s "श्रीङ्खला"))
    #:असिकिमु 
    ,(get-fun-arg (s:fn-with-name s "असिकिमु"))
    #:विषय 
    ,(get-fun-arg (s:fn-with-name s "विषय"))))

(define* (लेख #:key
             (शीर्षक #f)
             (उपशीर्षक #f)
             (लेखकहरू #f)
             (प्रकाशकको_नाम #f)
             (प्रकाशकको_ठेगाना #f)
             (प्रकाशनको_मिति #f)
             (संस्करण #f)
             (श्रीङ्खला #f)
             (असिकिमु #f)
             (विषय #f))
(display (list शीर्षक उपशीर्षक लेखकहरू प्रकाशकको_नाम प्रकाशकको_ठेगाना
               प्रकाशनको_मिति संस्करण श्रीङ्खला असिकिमु विषय)))

(define (print-dom d)
(if d (begin
        ;; do this to all doms
        (let ((root (s:root d)))
          (define lkh  (s:section-to-lisp root))
          (eval lkh (interaction-environment))
          )
        (newline)
        )))

(map print-dom
     (list dom dom-other))

(display "Freed all DOMs? : ")
(display (s:free-asts (list dom dom-other)))
(newline)

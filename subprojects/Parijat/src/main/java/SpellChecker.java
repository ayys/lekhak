import java.util.Set;
import java.util.HashSet;
import java.io.BufferedReader;
import java.io.FileReader;

public class SpellChecker {
	private static Set<String> wordSet;

	public static void loadWords() {
		try {
			BufferedReader in = new BufferedReader(new FileReader("words"));
			wordSet = new HashSet<String>();
			String word;
			
			while ((word = in.readLine()) != null) {
				wordSet.add(word);
			}
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
	}
	
	public static boolean check(String word) {
		return wordSet.contains(word);
	}
}

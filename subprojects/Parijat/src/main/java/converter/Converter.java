package converter;

import java.util.Arrays;

public class Converter {
	private static String[] unicode = new String[] {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z",
									"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
									"0","1","2","3","4","5","6","7","8","9",
									"~","`","!","@","#","$","%","^","&","*","(",")","-","_","+","[","{","]","}","\\","|",";","'","\"", ",","<",".",">","/","?","=","^","Î","å","÷"};

	public static String convertToUnicode(String s, String[] font) {
        try {
            return font[Arrays.asList(unicode).indexOf(s)];
        } catch (Exception exception) {
            return s;
        }
	}
}

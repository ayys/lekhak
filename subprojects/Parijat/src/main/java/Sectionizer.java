import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import javax.swing.JOptionPane;

public class Sectionizer {
	public static String sectionize(String html) {
		Document document = Jsoup.parse(html);
		Elements elements = document.body().children().select("*");
		for (Element element: elements) {
			try {
				if (!element.parent().ownText().equals("") || element == document.body().children().first() || element.previousElementSibling().tagName().equals("section")) {
					element.wrap("<section></section>");
					element.parent().addClass(element.nodeName() + " ");
				}
			} catch (Exception exception) {}
		}

		// for nested elements
		Elements sections = document.getElementsByTag("section");
		for (Element section: sections) {
			Element firstChildOfSection = section.child(0);	
			try {
				while (firstChildOfSection.ownText().equals("")) {
					if (firstChildOfSection.ownText().equals("")) {
						section.addClass(firstChildOfSection.child(0).nodeName() + " ");	
						firstChildOfSection = firstChildOfSection.child(0);
					}
				}
			} catch (Exception ex) {}
		}
		return document.toString();
	}

	public static void main(String[] args) {
		
		String html = "<html><head></head><body><p>This is a paragraph.</p><p>This is another paragraph</p></body></head><html>";
		sectionize(html);
	}
}

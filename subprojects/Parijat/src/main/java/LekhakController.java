import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.web.HTMLEditor;
import javafx.scene.control.ComboBox;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyEvent;
import javafx.collections.FXCollections;
import converter.PreetiConverter;
import javafx.scene.web.WebView;
import com.sun.javafx.webkit.Accessor;
import com.sun.webkit.WebPage;
import java.awt.Robot;
import javafx.scene.input.KeyCode;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

public class LekhakController implements Initializable {
    @FXML
    private HTMLEditor rtf;

	@FXML
	private ComboBox cmbFontFace;

	@FXML
	private Pane btnSpecialCharacters;

	private static WebView webView;
	private static WebPage webPage;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        hideToolbars(rtf);
		cmbFontFace.setItems(FXCollections.observableArrayList("Preeti", "Kantipur","Default"));
		cmbFontFace.getSelectionModel().selectFirst();
    }
    
    public void hideToolbars(final HTMLEditor editor) {
		editor.setVisible(false);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Node[] nodes = editor.lookupAll(".tool-bar").toArray(new Node[0]);
				for (Node node : nodes) {
					node.setVisible(false);
					node.setManaged(false);
				}
				editor.setVisible(true);
			}
		});
	}

	@FXML
	private void convertToUnicode(KeyEvent e) {
		// insert text at the position of cursor
		webView = (WebView)rtf.lookup("WebView");
		webPage = Accessor.getPageFor(webView.getEngine());  

		if (cmbFontFace.getSelectionModel().getSelectedItem().toString() != "Default") {
			webPage.executeCommand("insertText", PreetiConverter.convert(e.getText().charAt(0))); 
			backspace(e);
		}
	}

	private static void backspace(KeyEvent e) {
		if (!e.getCode().equals(KeyCode.ENTER)) {
			try {
				Robot robot = new Robot();
				robot.keyPress(java.awt.event.KeyEvent.VK_BACK_SPACE);
				robot.keyRelease(java.awt.event.KeyEvent.VK_BACK_SPACE);
			} catch (Exception ex) {}
		}
	}

	@FXML
	private void insertSpecial(ActionEvent e) {
		webView = (WebView) rtf.lookup("WebView");
		webPage = Accessor.getPageFor(webView.getEngine());
		webPage.executeCommand("insertText", ((Button)e.getSource()).getText());
	}

	@FXML
	private void toggleFonts(ActionEvent e) {
		if (!cmbFontFace.getSelectionModel().getSelectedItem().toString().equals("Default")) {
			btnSpecialCharacters.setVisible(true);
		} else {
			btnSpecialCharacters.setVisible(false);
		}
	}
}

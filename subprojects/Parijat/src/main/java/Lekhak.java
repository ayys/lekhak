import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Lekhak extends Application {
    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("Lekhak.fxml"));
		Scene scene = new Scene(parent);
        primaryStage.setScene(scene);
        primaryStage.show();
		primaryStage.setTitle("Siya Lekhak");
    }

	public static void main(String[] args) {
        launch(args);
    }
}

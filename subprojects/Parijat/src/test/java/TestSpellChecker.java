import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class TestSpellChecker {
	
	@Test
	public void testLoadWords() {
		SpellChecker.loadWords();
	}

	@Test
	public void testCheck() {
		SpellChecker.loadWords();

		//invalid word
		assertFalse(SpellChecker.check("apple"));

		//valid word
		assertTrue(SpellChecker.check("अ "));
	}
}

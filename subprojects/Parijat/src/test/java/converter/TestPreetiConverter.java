package converter;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestPreetiConverter {
	
	@Test
	public void testCovert() {
		//small letter
		assertEquals("ब", PreetiConverter.convert('a'));

		//capital letter
		assertEquals("ऋ", PreetiConverter.convert('C'));

		//numbers
		assertEquals("ढ", PreetiConverter.convert('9'));

		//symbols
		assertEquals("/", PreetiConverter.convert('÷'));

		//invalid value
		assertEquals("ढ", PreetiConverter.convert('ढ'));
	}
}

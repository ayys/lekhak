import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestSectionizer {
	
	@Test
	public void testSectionize() {
		
		//single elements
		String html = "<html><head></head><body><p>This is a paragraph.</p><p>This is another paragraph</p></body></head><html>";
		String sectionizedHtml = "<html>\n <head></head>\n <body>\n  <section class=\"p \">\n   <p>This is a paragraph.</p>\n  </section>\n  <section class=\"p \">\n   <p>This is another paragraph</p>\n  </section>\n </body>\n</html>";
		assertEquals(sectionizedHtml, Sectionizer.sectionize(html));

		//nested elements
		html = "<html><head></head><body><p><b><i>This is nested.</i></b></p></body></html>";
		sectionizedHtml = "<html>\n <head></head>\n <body>\n  <section class=\"p b i \">\n   <p><b><i>This is nested.</i></b></p>\n  </section>\n </body>\n</html>";
		assertEquals(sectionizedHtml, Sectionizer.sectionize(html));
	}
}

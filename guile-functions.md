# This file documents the guile functions used in sanga

* Note:
  - All functions/constants created for sanga must be prefixed with "s:".
  - [] before the function names specifies the language in which the said
    function is written - guile or C.

* Parse Files
	- [C function] s:parse-file *filename*
    Takes a filename and parses the sanga code in the said file.
	- [guile function] s:parse *filename* : __AST as Guile List__

* AST Related Functions
	- [C function] s:free-ast
	- [C function] s:root
	- [guile function] s:ast : __AST as Guile List__

* "Section Object Type" functions
	- [C function] s:parent *section* : __SECTION__
	- [C function] s:body *section* : __STRING__
	- [C function] s:offset *section* : __NUMBER__
	- [C function] s:nchildren *section* : __NUMBER__
	- [C function] s:next-sibling *section* : __SECTION__
	- [C function] s:first-child *section* : __SECTION__
	- [C function] s:nfunctions *section : __NUMBER__
	- [C function] s:function-list *section* : __FUNCTION LIST__
	- [C function] s:section-equal? *section1* *section2* : __BOOLEAN__
	- [guile function] s:functions *section* : __LIST__
	- [guile function] s:children *section : __LIST__
	- [guile function] s:children? *section* : __BOOLEAN__
	- [guile function] s:next-sibling? *section* : __BOOLEAN__
	- [guile function] s:parent? *section* : __BOOLEAN__

* "Function Object Type" functions
	- [C function] s:function-name *function* : __STRING__
	- [C function] s:function-argument *function* : __STRING__

सिया लेखक ‍                          {#mainpage}
============

__Lekhak Document Processor__ is a document writing software that will
help Nepali writers and journalists produce better content with
ease. Our aim is to provide a standard way to write documents in
Nepali. To that end, we will provide additional tools such has Nepali
dictionary, Spelling Checker, which will ease the creation process and
give better flow to the literature.

Alongside these tools, Lekhak will also provide a robust
font-to-Unicode conversion util- ity. With this utility, users can
type in any non-Unicode Nepali font they prefer, such as Preeti,
Kantipur, as well as Unicode fonts like Kalimati, Madan, and Lekhak
will make sure to store everything in the Unicode standard. This will
become immensely helpful when collaborating with many people on a
single document. The concept for Lekhak arose from a need to create a
decent Nepali document writing tool. The current word processors,
while powerful in their own respect, cannot cope with the nuisances of
the Nepali language.

Basic utilities, that English writers have become accustomed to, such
as grammar checkers, spelling checkers, are at best incomplete and
poorly implemented. We aim to fulfil the demand in the market for
production of beautiful Nepali documents. Our system uses the
time-tested TEX document format to produce beautiful, elegant
documents using a WYSIWYG (What You See Is What You Get) user
interface. Thus, one can say that Lekhak is the middle-ground between
writing TeX scripts, and using word processors. Still, describing
Lekhak as a front-end to TeX is not accurate. Lekhak uses TeX as one
of it’s output formats. Lekhak can output HTML, MS Open XML, SWG
Image, and a range of other formats. The goal of Lekhak is to fill the
gap between word processors and type-setting systems.


Installation
============

Lekhak is currently in development, so to install the system, first switch to
the `development` branch of the repository via `git checkout development`.
Then proceed according to your OS/Distro.

## Debian & Varients

```
# Goto the project directory
cd lekhak/
# Install dependencies
sudo apt install make cmake gcc guile guile-dev bison flex libflex

# Install sanga
cd subprojects/sanga/
mkdir build
cmake ..
sudo make install
```

## Fedora

```
# Goto the project directory
cd lekhak/
# install the dependencies
sudo dnf install guile guile-devel flex bison flex-devel bison-devel cmake make gcc
# Install Sanga
cd subprojects/sanga/
mkdir build
cmake ..
sudo make install
```

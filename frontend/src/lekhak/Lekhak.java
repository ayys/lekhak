
package lekhak;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.stage.Stage;

public class Lekhak extends Application {
    private HTMLEditor textEditor;
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Lekhak Document Processor");
        textEditor = new HTMLEditor();
        VBox vBox = new VBox();
        vBox.getChildren().add(textEditor);
        Scene scene = new Scene(vBox, 400, 300);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}